import 'bootstrap/dist/css/bootstrap.css';
import Vue from 'vue';
import VueResource from 'vue-resource';
import VueRouter from 'vue-router';
import App from './App.vue';
import { routes } from './routes';

Vue.use(VueRouter);
Vue.use(VueResource);

const router = new VueRouter({ routes });

new Vue({
  el: '#app',
  router,
  mode: 'history',
  render: h => h(App)
})


