import Home from './components/Home.vue'
import Insumo from './components/Insumo.vue'
import Emissor from './components/Emissor.vue'

export const routes = [
    {path: '', component: Home, title:''},
    {path: '/insumo', component: Insumo, title:'Insumo Cliente', menu:true},
    {path: '/emissor', component: Emissor, title:'Emissor Cliente', menu:true},
    // {path: '*', component: Home}
];